#!/usr/bin/awk -f

BEGIN {
	pn="firstrun";

	printf"{"
}

($1 != pn) {
	if (pn != "firstrun"){
		print "],"
	}
	printf "\""$1"\" : [\""$2"\""
}
($1 == pn) {
	printf ", \""$2"\""
}
(1){
	pn = $1
}

END {
	if(ln != "firstrun") {
		printf"]"
	}

	print"}"
}
